package com.hashami.cardano.v1.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class WalletAddress  {
  
  @ApiModelProperty(required = true, value = "Actual address.")
 /**
   * Actual address.  
  **/
  private String id = null;

  @ApiModelProperty(required = true, value = "True if this address has been used.")
 /**
   * True if this address has been used.  
  **/
  private Boolean used = null;

  @ApiModelProperty(required = true, value = "True if this address stores change from a previous transaction.")
 /**
   * True if this address stores change from a previous transaction.  
  **/
  private Boolean changeAddress = null;
 /**
   * Actual address.
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public WalletAddress id(String id) {
    this.id = id;
    return this;
  }

 /**
   * True if this address has been used.
   * @return used
  **/
  @JsonProperty("used")
  public Boolean isUsed() {
    return used;
  }

  public void setUsed(Boolean used) {
    this.used = used;
  }

  public WalletAddress used(Boolean used) {
    this.used = used;
    return this;
  }

 /**
   * True if this address stores change from a previous transaction.
   * @return changeAddress
  **/
  @JsonProperty("changeAddress")
  public Boolean isChangeAddress() {
    return changeAddress;
  }

  public void setChangeAddress(Boolean changeAddress) {
    this.changeAddress = changeAddress;
  }

  public WalletAddress changeAddress(Boolean changeAddress) {
    this.changeAddress = changeAddress;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WalletAddress {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    used: ").append(toIndentedString(used)).append("\n");
    sb.append("    changeAddress: ").append(toIndentedString(changeAddress)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

