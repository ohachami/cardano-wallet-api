package com.hashami.cardano.v1.dto;


import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class WalletUpdate  {
  

@XmlType(name="AssuranceLevelEnum")
@XmlEnum(String.class)
public enum AssuranceLevelEnum {

@XmlEnumValue("normal") normal(String.valueOf("normal")), @XmlEnumValue("strict") strict(String.valueOf("strict"));


    private String value;

    AssuranceLevelEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static AssuranceLevelEnum fromValue(String v) {
        for (AssuranceLevelEnum b : AssuranceLevelEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "New assurance level")
 /**
   * New assurance level  
  **/
  private AssuranceLevelEnum assuranceLevel = null;

  @ApiModelProperty(required = true, value = "New wallet's name")
 /**
   * New wallet's name  
  **/
  private String name = null;
 /**
   * New assurance level
   * @return assuranceLevel
  **/
  @JsonProperty("assuranceLevel")
  public String getAssuranceLevel() {
    if (assuranceLevel == null) {
      return null;
    }
    return assuranceLevel.value();
  }

  public void setAssuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
  }

  public WalletUpdate assuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
    return this;
  }

 /**
   * New wallet&#39;s name
   * @return name
  **/
  @JsonProperty("name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public WalletUpdate name(String name) {
    this.name = name;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WalletUpdate {\n");
    
    sb.append("    assuranceLevel: ").append(toIndentedString(assuranceLevel)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

