package com.hashami.cardano.v1.dto;



/**
 * Gets or Sets V1InputSelectionPolicy
 */
public enum V1InputSelectionPolicy {
  
	OptimizeForSecurity("OptimizeForSecurity"),
  
	OptimizeForHighThroughput("OptimizeForHighThroughput");

  private String value;

  V1InputSelectionPolicy(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static V1InputSelectionPolicy fromValue(String text) {
    for (V1InputSelectionPolicy b : V1InputSelectionPolicy.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  
}

