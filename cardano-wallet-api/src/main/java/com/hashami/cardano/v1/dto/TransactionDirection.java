package com.hashami.cardano.v1.dto;



/**
 * Gets or Sets TransactionDirection
 */
public enum TransactionDirection {
  
  OUTGOING("outgoing"),
  
  INCOMING("incoming");

  private String value;

  TransactionDirection(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static TransactionDirection fromValue(String text) {
    for (TransactionDirection b : TransactionDirection.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  
}

