package com.hashami.cardano.v1.dto;



/**
 * Gets or Sets WalletOperation
 */
public enum WalletOperation {
  
	create("create"),
  
	restore("restore");

  private String value;

  WalletOperation(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static WalletOperation fromValue(String text) {
    for (WalletOperation b : WalletOperation.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  
}

