package com.hashami.cardano.v1.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class V1SoftwareVersion  {
  
  @ApiModelProperty(required = true, value = "")
  private String applicationName = null;

  @ApiModelProperty(required = true, value = "")
  private Integer version = null;
 /**
   * Get applicationName
   * @return applicationName
  **/
  @JsonProperty("applicationName")
  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  public V1SoftwareVersion applicationName(String applicationName) {
    this.applicationName = applicationName;
    return this;
  }

 /**
   * Get version
   * minimum: 0
   * maximum: 4294967295
   * @return version
  **/
  @JsonProperty("version")
  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public V1SoftwareVersion version(Integer version) {
    this.version = version;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class V1SoftwareVersion {\n");
    
    sb.append("    applicationName: ").append(toIndentedString(applicationName)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

