package com.hashami.cardano.v1.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class EstimatedFees  {
  
  @ApiModelProperty(required = true, value = "Estimated fees, in ADA")
 /**
   * Estimated fees, in ADA  
  **/
  private BigDecimal estimatedAmount = null;
 /**
   * Estimated fees, in ADA
   * maximum: 45000000000000000
   * @return estimatedAmount
  **/
  @JsonProperty("estimatedAmount")
  public BigDecimal getEstimatedAmount() {
    return estimatedAmount;
  }

  public void setEstimatedAmount(BigDecimal estimatedAmount) {
    this.estimatedAmount = estimatedAmount;
  }

  public EstimatedFees estimatedAmount(BigDecimal estimatedAmount) {
    this.estimatedAmount = estimatedAmount;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EstimatedFees {\n");
    
    sb.append("    estimatedAmount: ").append(toIndentedString(estimatedAmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

