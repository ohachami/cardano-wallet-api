package com.hashami.cardano.v1.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Transaction  {
  
  @ApiModelProperty(required = true, value = "Transaction's id")
 /**
   * Transaction's id  
  **/
  private String id = null;

  @ApiModelProperty(required = true, value = "Number of confirmations")
 /**
   * Number of confirmations  
  **/
  private Integer confirmations = null;

  @ApiModelProperty(required = true, value = "Coins moved as part of the transaction, in ADA")
 /**
   * Coins moved as part of the transaction, in ADA  
  **/
  private BigDecimal amount = null;

  @ApiModelProperty(required = true, value = "One or more input money distributions")
 /**
   * One or more input money distributions  
  **/
  private List<PaymentDistribution> inputs = new ArrayList<PaymentDistribution>();

  @ApiModelProperty(required = true, value = "One or more ouputs money distributions")
 /**
   * One or more ouputs money distributions  
  **/
  private List<PaymentDistribution> outputs = new ArrayList<PaymentDistribution>();


@XmlType(name="TypeEnum")
@XmlEnum(String.class)
public enum TypeEnum {

@XmlEnumValue("local") LOCAL(String.valueOf("local")), @XmlEnumValue("foreign") FOREIGN(String.valueOf("foreign"));


    private String value;

    TypeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static TypeEnum fromValue(String v) {
        for (TypeEnum b : TypeEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "Type of transaction")
 /**
   * Type of transaction  
  **/
  private TypeEnum type = null;


@XmlType(name="DirectionEnum")
@XmlEnum(String.class)
public enum DirectionEnum {

@XmlEnumValue("outgoing") OUTGOING(String.valueOf("outgoing")), @XmlEnumValue("incoming") INCOMING(String.valueOf("incoming"));


    private String value;

    DirectionEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static DirectionEnum fromValue(String v) {
        for (DirectionEnum b : DirectionEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "Direction for this transaction")
 /**
   * Direction for this transaction  
  **/
  private DirectionEnum direction = null;
 /**
   * Transaction&#39;s id
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Transaction id(String id) {
    this.id = id;
    return this;
  }

 /**
   * Number of confirmations
   * minimum: 0
   * maximum: 384
   * @return confirmations
  **/
  @JsonProperty("confirmations")
  public Integer getConfirmations() {
    return confirmations;
  }

  public void setConfirmations(Integer confirmations) {
    this.confirmations = confirmations;
  }

  public Transaction confirmations(Integer confirmations) {
    this.confirmations = confirmations;
    return this;
  }

 /**
   * Coins moved as part of the transaction, in ADA
   * maximum: 45000000000000000
   * @return amount
  **/
  @JsonProperty("amount")
  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Transaction amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

 /**
   * One or more input money distributions
   * @return inputs
  **/
  @JsonProperty("inputs")
  public List<PaymentDistribution> getInputs() {
    return inputs;
  }

  public void setInputs(List<PaymentDistribution> inputs) {
    this.inputs = inputs;
  }

  public Transaction inputs(List<PaymentDistribution> inputs) {
    this.inputs = inputs;
    return this;
  }

  public Transaction addInputsItem(PaymentDistribution inputsItem) {
    this.inputs.add(inputsItem);
    return this;
  }

 /**
   * One or more ouputs money distributions
   * @return outputs
  **/
  @JsonProperty("outputs")
  public List<PaymentDistribution> getOutputs() {
    return outputs;
  }

  public void setOutputs(List<PaymentDistribution> outputs) {
    this.outputs = outputs;
  }

  public Transaction outputs(List<PaymentDistribution> outputs) {
    this.outputs = outputs;
    return this;
  }

  public Transaction addOutputsItem(PaymentDistribution outputsItem) {
    this.outputs.add(outputsItem);
    return this;
  }

 /**
   * Type of transaction
   * @return type
  **/
  @JsonProperty("type")
  public String getType() {
    if (type == null) {
      return null;
    }
    return type.value();
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public Transaction type(TypeEnum type) {
    this.type = type;
    return this;
  }

 /**
   * Direction for this transaction
   * @return direction
  **/
  @JsonProperty("direction")
  public String getDirection() {
    if (direction == null) {
      return null;
    }
    return direction.value();
  }

  public void setDirection(DirectionEnum direction) {
    this.direction = direction;
  }

  public Transaction direction(DirectionEnum direction) {
    this.direction = direction;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transaction {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    confirmations: ").append(toIndentedString(confirmations)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    inputs: ").append(toIndentedString(inputs)).append("\n");
    sb.append("    outputs: ").append(toIndentedString(outputs)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    direction: ").append(toIndentedString(direction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

