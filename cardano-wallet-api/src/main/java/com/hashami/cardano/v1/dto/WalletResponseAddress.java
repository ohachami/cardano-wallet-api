package com.hashami.cardano.v1.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class WalletResponseAddress  {
  
  @ApiModelProperty(required = true, value = "")
  private List<String> data = new ArrayList<String>();

  @ApiModelProperty(required = true, value = "")
  private ResponseStatus status = null;

  @ApiModelProperty(required = true, value = "")
  private Metadata meta = null;
 /**
   * Get data
   * @return data
  **/
  @JsonProperty("data")
  public List<String> getData() {
    return data;
  }

  public void setData(List<String> data) {
    this.data = data;
  }

  public WalletResponseAddress data(List<String> data) {
    this.data = data;
    return this;
  }

  public WalletResponseAddress addDataItem(String dataItem) {
    this.data.add(dataItem);
    return this;
  }

 /**
   * Get status
   * @return status
  **/
  @JsonProperty("status")
  public ResponseStatus getStatus() {
    return status;
  }

  public void setStatus(ResponseStatus status) {
    this.status = status;
  }

  public WalletResponseAddress status(ResponseStatus status) {
    this.status = status;
    return this;
  }

 /**
   * Get meta
   * @return meta
  **/
  @JsonProperty("meta")
  public Metadata getMeta() {
    return meta;
  }

  public void setMeta(Metadata meta) {
    this.meta = meta;
  }

  public WalletResponseAddress meta(Metadata meta) {
    this.meta = meta;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WalletResponseAddress {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

