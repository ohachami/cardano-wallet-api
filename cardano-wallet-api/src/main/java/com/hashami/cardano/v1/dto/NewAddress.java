package com.hashami.cardano.v1.dto;


import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class NewAddress  {
  
  @ApiModelProperty(value = "Optional spending password to unlock funds")
 /**
   * Optional spending password to unlock funds  
  **/
  private String spendingPassword = null;

  @ApiModelProperty(required = true, value = "Target account's index to store this address in")
 /**
   * Target account's index to store this address in  
  **/
  private BigDecimal accountIndex = null;

  @ApiModelProperty(required = true, value = "Corresponding wallet identifier")
 /**
   * Corresponding wallet identifier  
  **/
  private String walletId = null;
 /**
   * Optional spending password to unlock funds
   * @return spendingPassword
  **/
  @JsonProperty("spendingPassword")
  public String getSpendingPassword() {
    return spendingPassword;
  }

  public void setSpendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
  }

  public NewAddress spendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
    return this;
  }

 /**
   * Target account&#39;s index to store this address in
   * minimum: 0
   * maximum: 4294967295
   * @return accountIndex
  **/
  @JsonProperty("accountIndex")
  public BigDecimal getAccountIndex() {
    return accountIndex;
  }

  public void setAccountIndex(BigDecimal accountIndex) {
    this.accountIndex = accountIndex;
  }

  public NewAddress accountIndex(BigDecimal accountIndex) {
    this.accountIndex = accountIndex;
    return this;
  }

 /**
   * Corresponding wallet identifier
   * @return walletId
  **/
  @JsonProperty("walletId")
  public String getWalletId() {
    return walletId;
  }

  public void setWalletId(String walletId) {
    this.walletId = walletId;
  }

  public NewAddress walletId(String walletId) {
    this.walletId = walletId;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NewAddress {\n");
    
    sb.append("    spendingPassword: ").append(toIndentedString(spendingPassword)).append("\n");
    sb.append("    accountIndex: ").append(toIndentedString(accountIndex)).append("\n");
    sb.append("    walletId: ").append(toIndentedString(walletId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

