package com.hashami.cardano.v1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class NodeSettings  {
  
  @ApiModelProperty(required = true, value = "")
  private NodeSettingsSlotDuration slotDuration = null;

  @ApiModelProperty(required = true, value = "")
  private NodeSettingsSoftwareInfo softwareInfo = null;

  @ApiModelProperty(required = true, value = "Current project's version")
 /**
   * Current project's version  
  **/
  private String projectVersion = null;

  @ApiModelProperty(required = true, value = "Git revision of this deployment")
 /**
   * Git revision of this deployment  
  **/
  private String gitRevision = null;
 /**
   * Get slotDuration
   * @return slotDuration
  **/
  @JsonProperty("slotDuration")
  public NodeSettingsSlotDuration getSlotDuration() {
    return slotDuration;
  }

  public void setSlotDuration(NodeSettingsSlotDuration slotDuration) {
    this.slotDuration = slotDuration;
  }

  public NodeSettings slotDuration(NodeSettingsSlotDuration slotDuration) {
    this.slotDuration = slotDuration;
    return this;
  }

 /**
   * Get softwareInfo
   * @return softwareInfo
  **/
  @JsonProperty("softwareInfo")
  public NodeSettingsSoftwareInfo getSoftwareInfo() {
    return softwareInfo;
  }

  public void setSoftwareInfo(NodeSettingsSoftwareInfo softwareInfo) {
    this.softwareInfo = softwareInfo;
  }

  public NodeSettings softwareInfo(NodeSettingsSoftwareInfo softwareInfo) {
    this.softwareInfo = softwareInfo;
    return this;
  }

 /**
   * Current project&#39;s version
   * @return projectVersion
  **/
  @JsonProperty("projectVersion")
  public String getProjectVersion() {
    return projectVersion;
  }

  public void setProjectVersion(String projectVersion) {
    this.projectVersion = projectVersion;
  }

  public NodeSettings projectVersion(String projectVersion) {
    this.projectVersion = projectVersion;
    return this;
  }

 /**
   * Git revision of this deployment
   * @return gitRevision
  **/
  @JsonProperty("gitRevision")
  public String getGitRevision() {
    return gitRevision;
  }

  public void setGitRevision(String gitRevision) {
    this.gitRevision = gitRevision;
  }

  public NodeSettings gitRevision(String gitRevision) {
    this.gitRevision = gitRevision;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NodeSettings {\n");
    
    sb.append("    slotDuration: ").append(toIndentedString(slotDuration)).append("\n");
    sb.append("    softwareInfo: ").append(toIndentedString(softwareInfo)).append("\n");
    sb.append("    projectVersion: ").append(toIndentedString(projectVersion)).append("\n");
    sb.append("    gitRevision: ").append(toIndentedString(gitRevision)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

