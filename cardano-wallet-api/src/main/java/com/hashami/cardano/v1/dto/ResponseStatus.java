package com.hashami.cardano.v1.dto;



/**
 * Gets or Sets ResponseStatus
 */
public enum ResponseStatus {
  
	success("success"),
	  
	fail("fail"),
  
	error("error");

  private String value;

  ResponseStatus(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static ResponseStatus fromValue(String text) {
    for (ResponseStatus b : ResponseStatus.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  
}

