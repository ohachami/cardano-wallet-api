package com.hashami.cardano.v1.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class NewAccount  {
  
  @ApiModelProperty(value = "Optional spending password to unlock funds")
 /**
   * Optional spending password to unlock funds  
  **/
  private String spendingPassword = null;

  @ApiModelProperty(required = true, value = "Account's name")
 /**
   * Account's name  
  **/
  private String name = null;
 /**
   * Optional spending password to unlock funds
   * @return spendingPassword
  **/
  @JsonProperty("spendingPassword")
  public String getSpendingPassword() {
    return spendingPassword;
  }

  public void setSpendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
  }

  public NewAccount spendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
    return this;
  }

 /**
   * Account&#39;s name
   * @return name
  **/
  @JsonProperty("name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public NewAccount name(String name) {
    this.name = name;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NewAccount {\n");
    
    sb.append("    spendingPassword: ").append(toIndentedString(spendingPassword)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

