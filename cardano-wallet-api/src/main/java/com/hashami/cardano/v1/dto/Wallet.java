package com.hashami.cardano.v1.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Wallet  {
  
  @ApiModelProperty(required = true, value = "Unique wallet identifier.")
 /**
   * Unique wallet identifier.  
  **/
  private String id = null;

  @ApiModelProperty(required = true, value = "Wallet's name.")
 /**
   * Wallet's name.  
  **/
  private String name = null;

  @ApiModelProperty(required = true, value = "Current balance, in Lovelace.")
 /**
   * Current balance, in Lovelace.  
  **/
  private BigDecimal balance = null;

  @ApiModelProperty(required = true, value = "Whether or not the wallet has a passphrase.")
 /**
   * Whether or not the wallet has a passphrase.  
  **/
  private Boolean hasSpendingPassword = null;

  @ApiModelProperty(required = true, value = "The timestamp that the passphrase was last updated.")
 /**
   * The timestamp that the passphrase was last updated.  
  **/
  private Date spendingPasswordLastUpdate = null;

  @ApiModelProperty(required = true, value = "The timestamp that the wallet was created.")
 /**
   * The timestamp that the wallet was created.  
  **/
  private Date createdAt = null;


@XmlType(name="AssuranceLevelEnum")
@XmlEnum(String.class)
public enum AssuranceLevelEnum {

@XmlEnumValue("normal") normal(String.valueOf("normal")), @XmlEnumValue("strict") strict(String.valueOf("strict"));


    private String value;

    AssuranceLevelEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static AssuranceLevelEnum fromValue(String v) {
        for (AssuranceLevelEnum b : AssuranceLevelEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "The assurance level of the wallet.")
 /**
   * The assurance level of the wallet.  
  **/
  private AssuranceLevelEnum assuranceLevel = null;

  @ApiModelProperty(required = true, value = "")
  private WalletSyncState syncState = null;
 /**
   * Unique wallet identifier.
   * @return id
  **/
  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Wallet id(String id) {
    this.id = id;
    return this;
  }

 /**
   * Wallet&#39;s name.
   * @return name
  **/
  @JsonProperty("name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Wallet name(String name) {
    this.name = name;
    return this;
  }

 /**
   * Current balance, in Lovelace.
   * maximum: 45000000000000000
   * @return balance
  **/
  @JsonProperty("balance")
  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public Wallet balance(BigDecimal balance) {
    this.balance = balance;
    return this;
  }

 /**
   * Whether or not the wallet has a passphrase.
   * @return hasSpendingPassword
  **/
  @JsonProperty("hasSpendingPassword")
  public Boolean isHasSpendingPassword() {
    return hasSpendingPassword;
  }

  public void setHasSpendingPassword(Boolean hasSpendingPassword) {
    this.hasSpendingPassword = hasSpendingPassword;
  }

  public Wallet hasSpendingPassword(Boolean hasSpendingPassword) {
    this.hasSpendingPassword = hasSpendingPassword;
    return this;
  }

 /**
   * The timestamp that the passphrase was last updated.
   * @return spendingPasswordLastUpdate
  **/
  @JsonProperty("spendingPasswordLastUpdate")
  public Date getSpendingPasswordLastUpdate() {
    return spendingPasswordLastUpdate;
  }

  public void setSpendingPasswordLastUpdate(Date spendingPasswordLastUpdate) {
    this.spendingPasswordLastUpdate = spendingPasswordLastUpdate;
  }

  public Wallet spendingPasswordLastUpdate(Date spendingPasswordLastUpdate) {
    this.spendingPasswordLastUpdate = spendingPasswordLastUpdate;
    return this;
  }

 /**
   * The timestamp that the wallet was created.
   * @return createdAt
  **/
  @JsonProperty("createdAt")
  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Wallet createdAt(Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

 /**
   * The assurance level of the wallet.
   * @return assuranceLevel
  **/
  @JsonProperty("assuranceLevel")
  public String getAssuranceLevel() {
    if (assuranceLevel == null) {
      return null;
    }
    return assuranceLevel.value();
  }

  public void setAssuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
  }

  public Wallet assuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
    return this;
  }

 /**
   * Get syncState
   * @return syncState
  **/
  @JsonProperty("syncState")
  public WalletSyncState getSyncState() {
    return syncState;
  }

  public void setSyncState(WalletSyncState syncState) {
    this.syncState = syncState;
  }

  public Wallet syncState(WalletSyncState syncState) {
    this.syncState = syncState;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Wallet {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("    hasSpendingPassword: ").append(toIndentedString(hasSpendingPassword)).append("\n");
    sb.append("    spendingPasswordLastUpdate: ").append(toIndentedString(spendingPasswordLastUpdate)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    assuranceLevel: ").append(toIndentedString(assuranceLevel)).append("\n");
    sb.append("    syncState: ").append(toIndentedString(syncState)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

