package com.hashami.cardano.v1.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
  * The sync state for this wallet.
 **/
@ApiModel(description="The sync state for this wallet.")
public class WalletSyncState  {
  

@XmlType(name="TagEnum")
@XmlEnum(String.class)
public enum TagEnum {

@XmlEnumValue("restoring") restoring(String.valueOf("restoring")), @XmlEnumValue("synced") synced(String.valueOf("synced"));


    private String value;

    TagEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static TagEnum fromValue(String v) {
        for (TagEnum b : TagEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(required = true, value = "")
  private TagEnum tag = null;

  @ApiModelProperty(value = "")
  private SyncProgress data = null;
 /**
   * Get tag
   * @return tag
  **/
  @JsonProperty("tag")
  public String getTag() {
    if (tag == null) {
      return null;
    }
    return tag.value();
  }

  public void setTag(TagEnum tag) {
    this.tag = tag;
  }

  public WalletSyncState tag(TagEnum tag) {
    this.tag = tag;
    return this;
  }

 /**
   * Get data
   * @return data
  **/
  @JsonProperty("data")
  public SyncProgress getData() {
    return data;
  }

  public void setData(SyncProgress data) {
    this.data = data;
  }

  public WalletSyncState data(SyncProgress data) {
    this.data = data;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WalletSyncState {\n");
    
    sb.append("    tag: ").append(toIndentedString(tag)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

