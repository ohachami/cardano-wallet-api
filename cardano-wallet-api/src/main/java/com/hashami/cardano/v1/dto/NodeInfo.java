package com.hashami.cardano.v1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class NodeInfo  {
  
  @ApiModelProperty(required = true, value = "")
  private NodeInfoSyncProgress syncProgress = null;

  @ApiModelProperty(value = "")
  private NodeInfoBlockchainHeight blockchainHeight = null;

  @ApiModelProperty(required = true, value = "")
  private NodeInfoLocalBlockchainHeight localBlockchainHeight = null;

  @ApiModelProperty(required = true, value = "")
  private NodeInfoLocalTimeDifference localTimeDifference = null;
 /**
   * Get syncProgress
   * @return syncProgress
  **/
  @JsonProperty("syncProgress")
  public NodeInfoSyncProgress getSyncProgress() {
    return syncProgress;
  }

  public void setSyncProgress(NodeInfoSyncProgress syncProgress) {
    this.syncProgress = syncProgress;
  }

  public NodeInfo syncProgress(NodeInfoSyncProgress syncProgress) {
    this.syncProgress = syncProgress;
    return this;
  }

 /**
   * Get blockchainHeight
   * @return blockchainHeight
  **/
  @JsonProperty("blockchainHeight")
  public NodeInfoBlockchainHeight getBlockchainHeight() {
    return blockchainHeight;
  }

  public void setBlockchainHeight(NodeInfoBlockchainHeight blockchainHeight) {
    this.blockchainHeight = blockchainHeight;
  }

  public NodeInfo blockchainHeight(NodeInfoBlockchainHeight blockchainHeight) {
    this.blockchainHeight = blockchainHeight;
    return this;
  }

 /**
   * Get localBlockchainHeight
   * @return localBlockchainHeight
  **/
  @JsonProperty("localBlockchainHeight")
  public NodeInfoLocalBlockchainHeight getLocalBlockchainHeight() {
    return localBlockchainHeight;
  }

  public void setLocalBlockchainHeight(NodeInfoLocalBlockchainHeight localBlockchainHeight) {
    this.localBlockchainHeight = localBlockchainHeight;
  }

  public NodeInfo localBlockchainHeight(NodeInfoLocalBlockchainHeight localBlockchainHeight) {
    this.localBlockchainHeight = localBlockchainHeight;
    return this;
  }

 /**
   * Get localTimeDifference
   * @return localTimeDifference
  **/
  @JsonProperty("localTimeDifference")
  public NodeInfoLocalTimeDifference getLocalTimeDifference() {
    return localTimeDifference;
  }

  public void setLocalTimeDifference(NodeInfoLocalTimeDifference localTimeDifference) {
    this.localTimeDifference = localTimeDifference;
  }

  public NodeInfo localTimeDifference(NodeInfoLocalTimeDifference localTimeDifference) {
    this.localTimeDifference = localTimeDifference;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NodeInfo {\n");
    
    sb.append("    syncProgress: ").append(toIndentedString(syncProgress)).append("\n");
    sb.append("    blockchainHeight: ").append(toIndentedString(blockchainHeight)).append("\n");
    sb.append("    localBlockchainHeight: ").append(toIndentedString(localBlockchainHeight)).append("\n");
    sb.append("    localTimeDifference: ").append(toIndentedString(localTimeDifference)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

