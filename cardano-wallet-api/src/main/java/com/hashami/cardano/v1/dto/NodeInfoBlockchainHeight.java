package com.hashami.cardano.v1.dto;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
  * If known, the current blockchain height, in number of blocks
 **/
@ApiModel(description="If known, the current blockchain height, in number of blocks")
public class NodeInfoBlockchainHeight  {
  
  @ApiModelProperty(required = true, value = "")
  private BigDecimal quantity = null;


@XmlType(name="UnitEnum")
@XmlEnum(String.class)
public enum UnitEnum {

@XmlEnumValue("blocks") BLOCKS(String.valueOf("blocks"));


    private String value;

    UnitEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static UnitEnum fromValue(String v) {
        for (UnitEnum b : UnitEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(value = "")
  private UnitEnum unit = null;
 /**
   * Get quantity
   * minimum: 0
   * maximum: 18446744073709552000
   * @return quantity
  **/
  @JsonProperty("quantity")
  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public NodeInfoBlockchainHeight quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

 /**
   * Get unit
   * @return unit
  **/
  @JsonProperty("unit")
  public String getUnit() {
    if (unit == null) {
      return null;
    }
    return unit.value();
  }

  public void setUnit(UnitEnum unit) {
    this.unit = unit;
  }

  public NodeInfoBlockchainHeight unit(UnitEnum unit) {
    this.unit = unit;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NodeInfoBlockchainHeight {\n");
    
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    unit: ").append(toIndentedString(unit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

