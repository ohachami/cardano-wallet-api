package com.hashami.cardano.v1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
  * Source for the payment
 **/
@ApiModel(description="Source for the payment")
public class PaymentSource  {
  
  @ApiModelProperty(required = true, value = "Corresponding account's index on the wallet")
 /**
   * Corresponding account's index on the wallet  
  **/
  private Integer accountIndex = null;

  @ApiModelProperty(required = true, value = "Target wallet identifier to reach")
 /**
   * Target wallet identifier to reach  
  **/
  private String walletId = null;
 /**
   * Corresponding account&#39;s index on the wallet
   * minimum: 0
   * maximum: 4294967295
   * @return accountIndex
  **/
  @JsonProperty("accountIndex")
  public Integer getAccountIndex() {
    return accountIndex;
  }

  public void setAccountIndex(Integer accountIndex) {
    this.accountIndex = accountIndex;
  }

  public PaymentSource accountIndex(Integer accountIndex) {
    this.accountIndex = accountIndex;
    return this;
  }

 /**
   * Target wallet identifier to reach
   * @return walletId
  **/
  @JsonProperty("walletId")
  public String getWalletId() {
    return walletId;
  }

  public void setWalletId(String walletId) {
    this.walletId = walletId;
  }

  public PaymentSource walletId(String walletId) {
    this.walletId = walletId;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentSource {\n");
    
    sb.append("    accountIndex: ").append(toIndentedString(accountIndex)).append("\n");
    sb.append("    walletId: ").append(toIndentedString(walletId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

