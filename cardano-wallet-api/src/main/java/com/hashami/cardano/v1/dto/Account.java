package com.hashami.cardano.v1.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Account  {
  
  @ApiModelProperty(required = true, value = "Account's index in the wallet, starting at 0.")
 /**
   * Account's index in the wallet, starting at 0.  
  **/
  private Long index = null;

  @ApiModelProperty(required = true, value = "Public addresses pointing to this account.")
 /**
   * Public addresses pointing to this account.  
  **/
  private List<WalletAddress> addresses = new ArrayList<WalletAddress>();

  @ApiModelProperty(required = true, value = "Available funds, in Lovelace.")
 /**
   * Available funds, in Lovelace.  
  **/
  private BigDecimal amount = null;

  @ApiModelProperty(required = true, value = "Account's name.")
 /**
   * Account's name.  
  **/
  private String name = null;

  @ApiModelProperty(required = true, value = "Id of the wallet this account belongs to.")
 /**
   * Id of the wallet this account belongs to.  
  **/
  private String walletId = null;
 /**
   * Account&#39;s index in the wallet, starting at 0.
   * minimum: 0
   * maximum: 4294967295
   * @return index
  **/
  @JsonProperty("index")
  public Long getIndex() {
    return index;
  }

  public void setIndex(Long index) {
    this.index = index;
  }

  public Account index(Long index) {
    this.index = index;
    return this;
  }

 /**
   * Public addresses pointing to this account.
   * @return addresses
  **/
  @JsonProperty("addresses")
  public List<WalletAddress> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<WalletAddress> addresses) {
    this.addresses = addresses;
  }

  public Account addresses(List<WalletAddress> addresses) {
    this.addresses = addresses;
    return this;
  }

  public Account addAddressesItem(WalletAddress addressesItem) {
    this.addresses.add(addressesItem);
    return this;
  }

 /**
   * Available funds, in Lovelace.
   * maximum: 45000000000000000
   * @return amount
  **/
  @JsonProperty("amount")
  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Account amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

 /**
   * Account&#39;s name.
   * @return name
  **/
  @JsonProperty("name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Account name(String name) {
    this.name = name;
    return this;
  }

 /**
   * Id of the wallet this account belongs to.
   * @return walletId
  **/
  @JsonProperty("walletId")
  public String getWalletId() {
    return walletId;
  }

  public void setWalletId(String walletId) {
    this.walletId = walletId;
  }

  public Account walletId(String walletId) {
    this.walletId = walletId;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Account {\n");
    
    sb.append("    index: ").append(toIndentedString(index)).append("\n");
    sb.append("    addresses: ").append(toIndentedString(addresses)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    walletId: ").append(toIndentedString(walletId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

