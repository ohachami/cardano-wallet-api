package com.hashami.cardano.v1.dto;



/**
 * Gets or Sets TransactionType
 */
public enum TransactionType {
  
  LOCAL("local"),
  
  FOREIGN("foreign");

  private String value;

  TransactionType(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static TransactionType fromValue(String text) {
    for (TransactionType b : TransactionType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  
}

