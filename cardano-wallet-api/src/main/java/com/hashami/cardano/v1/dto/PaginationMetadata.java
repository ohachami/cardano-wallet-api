package com.hashami.cardano.v1.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class PaginationMetadata  {
  
  @ApiModelProperty(required = true, value = "")
  private BigDecimal totalPages = null;

  @ApiModelProperty(required = true, value = "")
  private Integer page = null;

  @ApiModelProperty(required = true, value = "")
  private Integer perPage = null;

  @ApiModelProperty(required = true, value = "")
  private BigDecimal totalEntries = null;
 /**
   * Get totalPages
   * minimum: 0
   * maximum: 9223372036854776000
   * @return totalPages
  **/
  @JsonProperty("totalPages")
  public BigDecimal getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(BigDecimal totalPages) {
    this.totalPages = totalPages;
  }

  public PaginationMetadata totalPages(BigDecimal totalPages) {
    this.totalPages = totalPages;
    return this;
  }

 /**
   * Get page
   * @return page
  **/
  @JsonProperty("page")
  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public PaginationMetadata page(Integer page) {
    this.page = page;
    return this;
  }

 /**
   * Get perPage
   * @return perPage
  **/
  @JsonProperty("perPage")
  public Integer getPerPage() {
    return perPage;
  }

  public void setPerPage(Integer perPage) {
    this.perPage = perPage;
  }

  public PaginationMetadata perPage(Integer perPage) {
    this.perPage = perPage;
    return this;
  }

 /**
   * Get totalEntries
   * minimum: 0
   * maximum: 9223372036854776000
   * @return totalEntries
  **/
  @JsonProperty("totalEntries")
  public BigDecimal getTotalEntries() {
    return totalEntries;
  }

  public void setTotalEntries(BigDecimal totalEntries) {
    this.totalEntries = totalEntries;
  }

  public PaginationMetadata totalEntries(BigDecimal totalEntries) {
    this.totalEntries = totalEntries;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaginationMetadata {\n");
    
    sb.append("    totalPages: ").append(toIndentedString(totalPages)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    perPage: ").append(toIndentedString(perPage)).append("\n");
    sb.append("    totalEntries: ").append(toIndentedString(totalEntries)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

