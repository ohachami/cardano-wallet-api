package com.hashami.cardano.v1.dto;

import java.util.ArrayList;

public class V1BackupPhrase extends ArrayList<String> {
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class V1BackupPhrase {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

