package com.hashami.cardano.v1.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class PasswordUpdate  {
  
  @ApiModelProperty(required = true, value = "Old password")
 /**
   * Old password  
  **/
  private String old = null;

  @ApiModelProperty(required = true, value = "New passowrd")
 /**
   * New passowrd  
  **/
  private String _new = null;
 /**
   * Old password
   * @return old
  **/
  @JsonProperty("old")
  public String getOld() {
    return old;
  }

  public void setOld(String old) {
    this.old = old;
  }

  public PasswordUpdate old(String old) {
    this.old = old;
    return this;
  }

 /**
   * New passowrd
   * @return _new
  **/
  @JsonProperty("new")
  public String getNew() {
    return _new;
  }

  public void setNew(String _new) {
    this._new = _new;
  }

  public PasswordUpdate _new(String _new) {
    this._new = _new;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordUpdate {\n");
    
    sb.append("    old: ").append(toIndentedString(old)).append("\n");
    sb.append("    _new: ").append(toIndentedString(_new)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

