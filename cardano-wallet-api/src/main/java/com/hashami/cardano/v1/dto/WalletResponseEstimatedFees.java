package com.hashami.cardano.v1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class WalletResponseEstimatedFees  {
  
  @ApiModelProperty(required = true, value = "")
  private EstimatedFees data = null;

  @ApiModelProperty(required = true, value = "")
  private ResponseStatus status = null;

  @ApiModelProperty(required = true, value = "")
  private Metadata meta = null;
 /**
   * Get data
   * @return data
  **/
  @JsonProperty("data")
  public EstimatedFees getData() {
    return data;
  }

  public void setData(EstimatedFees data) {
    this.data = data;
  }

  public WalletResponseEstimatedFees data(EstimatedFees data) {
    this.data = data;
    return this;
  }

 /**
   * Get status
   * @return status
  **/
  @JsonProperty("status")
  public ResponseStatus getStatus() {
    return status;
  }

  public void setStatus(ResponseStatus status) {
    this.status = status;
  }

  public WalletResponseEstimatedFees status(ResponseStatus status) {
    this.status = status;
    return this;
  }

 /**
   * Get meta
   * @return meta
  **/
  @JsonProperty("meta")
  public Metadata getMeta() {
    return meta;
  }

  public void setMeta(Metadata meta) {
    this.meta = meta;
  }

  public WalletResponseEstimatedFees meta(Metadata meta) {
    this.meta = meta;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WalletResponseEstimatedFees {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

