package com.hashami.cardano.v1.dto;



/**
 * Gets or Sets AssuranceLevel
 */
public enum AssuranceLevel {
  
  NORMAL("normal"),
  
  STRICT("strict");

  private String value;

  AssuranceLevel(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static AssuranceLevel fromValue(String text) {
    for (AssuranceLevel b : AssuranceLevel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  
}

